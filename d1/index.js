//how to create a basic server
//Use the 'require' directive to load Node JS modules
//A 'Module' is a sfotware component or part of a program that contains one or more routines

let http = require("http");
//The "HTTP module" can transfer data using the Hyper Text Transfer Protocol
//it is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
//HTTP is a protocol that allows the fetching of resources such as HTML documents.
//Client (browser) and server (nodeJS) communicate by exchanging individual messages.
//The messages sent by the client, usually a Web browser, are called "requests"
//The messages sent by the server as an answer are called "responses"

//Using this module's createServer() method, we can create an HTTP server that listens to requets on a specified adress/URL/port and gives responses back to the client
//First variable is always a requesgt, response is the second
http.createServer(function(request, response){
	response.writeHead(200,{'Content-type':'text/plain'});
	//the 200 is a status code, see this document for the details of the status codes (https://developer.mozilla.org/en-US/docs/Web/HTTP/Status), 

	//'Content-type', sets the content type of the response as a plain text message. This can be changed to 'image/jpeg'.What we always use for node is 'application/json', which means we would send json data to the client side.

	response.end("Hello World")
	//This is to break the response of our server. Can be blank

}).listen(4000)
//.response is from our variable response and uses methods available. It is a response to client

console.log('Server running at localhost:4000')
//http://localhost:4000
//The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that re sent to our server.

//called the http as variable and create a server.
//we need to add a task to the createServer through a function()
//we need to name the request and response as variables, can be anyhting you want
//.listen() listens to the port to receive request and send a response
//	A port is a virtual point where network connections start and end
//	Each port is associated with a specified process or service