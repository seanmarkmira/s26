const http = require("http");

//Create a variable "port" to store the port number
const port = 4000

//Create a variable "server" that stores the output of the "createServer" method
const server = http.createServer((request, response)=>{
	//accessing the 'greeting' route that returns a message of "Hello Again"
	//http://localhost:4000/greeting
	//"request" is an object that is sent via the client(browser)
	//The "url" property refers to the url or the link in the browser
	if(request.url == '/greeting'){
		response.writeHead(200,{'Content-type': 'text/plain'})
		response.end('Hello Again');
	}else if(request.url == '/homepage'){
		response.writeHead(200,{'Content-type': 'text/plain'})
		response.end("this is the homepage")
	}else{
		response.writeHead(404,{'Content-type': 'text/plain'})
		response.end("page not found")
	}
	//Access the "homepage" route and return a message of "this is the homepage" with a status code 200 and a plain text
	//All other routes will return a message of "page not available". Give a status code for not found and a plain text
})

server.listen(port)

//When server is running, console will printe the message:
console.log(`Server now accessible at localhost:${port}`);