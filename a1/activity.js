// The questions are as follows:

// 1 What directive is used by Node.js in loading the modules it needs?
const http = require("http");
-- The require portion of the code above

// 2 What Node.js module contains a method for server creation?
-- http

// 3 What is the method of the http object responsible for creating a server using Node.js?
const server = http.createServer((request, response)=>{})
-- the createServer method is responsible for creating a server in nodejs

// 4 What method of the response object allows us to set status codes and content types?
response.writeHead(200,{'Content-type': 'text/plain'})
-- In the writeHead method, we can set the status code as the first parameter in the method

// 5 Where will console.log() output its contents when run in Node.js?
-- In the terminal where we have executed node `filename.js`

// 6 What property of the request object contains the address's endpoint?
-- request.url